from conans import ConanFile, CMake, tools
import shutil

class LurlparserConan(ConanFile):
    name            = "lurlparser"
    version         = "1.1"
    license         = "MIT"
    author          = "toge.mail@gmail.com"
    homepage        = "https://github.com/corporateshark/LUrlParser/"
    url             = "https://bitbucket.org/toge/conan-lurlparser/src"
    description     = "Lightweight URL         &             URI    parser (RFC 1738, RFC 3986) "
    topics          = ("RFC1738",  "RFC3986",  "URI",        "URL", "parser")
    settings        = "os",        "compiler", "build_type", "arch"
    generators      = "cmake"
    exports         = ["CMakeLists.txt"]

    def source(self):
        tools.get("https://github.com/corporateshark/LUrlParser/archive/release-{}.zip".format(self.version))
        shutil.move("LUrlParser-release-{}".format(self.version), "LUrlParser")
        shutil.move("CMakeLists.txt", "LUrlParser/CMakeLists.txt")

    def build(self):
        cmake = CMake(self)
        cmake.configure(source_folder="LUrlParser")
        cmake.build()

    def package(self):
        self.copy("*.h",     dst="include", src="LUrlParser")
        self.copy("*.lib",   dst="lib",     keep_path=False)
        self.copy("*.dll",   dst="bin",     keep_path=False)
        self.copy("*.so",    dst="lib",     keep_path=False)
        self.copy("*.dylib", dst="lib",     keep_path=False)
        self.copy("*.a",     dst="lib",     keep_path=False)

    def package_info(self):
        self.cpp_info.libs = ["lurlparser"]
